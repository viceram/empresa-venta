package material;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;

import org.junit.Test;

import empresa.excepcion.ValorNegativoCeroException;
import empresa.material.MaterialFabricado;

public class MaterialFabricadoTest {
    @Test
    public void obtenerPrecioVenta() throws ValorNegativoCeroException {
        MaterialFabricado mF = new MaterialFabricado("1", "mT", new BigDecimal(300), 20, "descripcion");
        BigDecimal bg = new BigDecimal(7200);
        assertEquals(bg.setScale(2), mF.getPrecio());
    }

    @Test (expected = ValorNegativoCeroException.class)
    public void precioNegativo() throws ValorNegativoCeroException {
        MaterialFabricado mF = new MaterialFabricado("1", "mT", new BigDecimal(-233), 20, "descripcion");
    }
}
