package empresa.facturacion;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import empresa.herramienta.*;
import empresa.material.*;
import empresa.excepcion.*;
import empresa.auxiliar.*;

public class OrdenDeVentaTest {

    OrdenDeVenta orden1;
    LocalDateTime horaFechaDeEmision;

    @Before
    public void before() {

        horaFechaDeEmision = LocalDateTime.now();
        orden1 = new OrdenDeVenta(horaFechaDeEmision, 1);
    }

    @Test
    public void testaAregarItem() throws ItemYaFigurandoEnOrdenException, ValorNegativoCeroException {

        BigDecimal precio = new BigDecimal(1300.00);
        MaterialFabricado material1 = new MaterialFabricado("00", "Ladrillo", precio, 230,
                "Hecho con material de calidad");
        BigDecimal precio2 = new BigDecimal(2300.00);
        HerramientaElectrica herramienta1 = new HerramientaElectrica("122", "Taladro", precio2, 1, 500.25);
        orden1.agregarItem(material1);
        orden1.agregarItem(herramienta1);

        assertEquals(2, orden1.getItemsVendidos().size());
    }

    @Test
    public void testModificarItem() throws ItemYaFigurandoEnOrdenException, ItemNoRegistradoException,
            ValorNegativoCeroException {

        BigDecimal precio = new BigDecimal(1300.00);
        MaterialFabricado material1 = new MaterialFabricado("00", "Ladrillo", precio, 230,
                "Hecho con material de calidad");
        orden1.agregarItem(material1);
        BigDecimal precio2 = new BigDecimal(690.00);
        MaterialFabricado material2 = new MaterialFabricado("00", "Ladrillo", precio2, 100,
                "Hecho con material de calidad");
        orden1.modificarItemVendido(material2);
        ItemParaVenta itemObtenido = orden1.getItem("00");

        assertEquals(material2, itemObtenido);
    }

    @Test(expected = ItemYaFigurandoEnOrdenException.class)
    public void resgistrarDosDelMismoItem() throws ItemYaFigurandoEnOrdenException, ValorNegativoCeroException {
        BigDecimal precio = new BigDecimal(1300.00);
        MaterialFabricado material1 = new MaterialFabricado("00", "Ladrillo", precio, 230,
                "Hecho con material de calidad");
        orden1.agregarItem(material1);
        orden1.agregarItem(material1);
    }

    @Test(expected = ItemNoRegistradoException.class)
    public void modificarItemNoVendido() throws ItemYaFigurandoEnOrdenException, ItemNoRegistradoException,
            ValorNegativoCeroException {
        BigDecimal precio = new BigDecimal(1300.00);
        MaterialFabricado material1 = new MaterialFabricado("00", "Ladrillo", precio, 230,
                "Hecho con material de calidad");
        BigDecimal precio2 = new BigDecimal(2300.00);
        HerramientaElectrica herramienta1 = new HerramientaElectrica("122", "Taladro", precio2, 1, 500.25);
        orden1.agregarItem(material1);
        orden1.modificarItemVendido(herramienta1);
    }

    @Test 
    public void ordenarPorPrecio() throws ItemYaFigurandoEnOrdenException, ValorNegativoCeroException {
        BigDecimal precio = new BigDecimal(1300.00);
        MaterialFabricado material1 = new MaterialFabricado("00", "Ladrillo", precio, 230,
                "Hecho con material de calidad");
        BigDecimal precio2 = new BigDecimal(2300.00);
        HerramientaElectrica herramienta1 = new HerramientaElectrica("122", "Taladro", precio2, 2, 500.25);
        BigDecimal precio3 = new BigDecimal(650.00);
        HerramientaManual herramienta2 = new HerramientaManual("111", "Martillo", precio3, 12,"clavar y desclavar");
        orden1.agregarItem(material1);
        orden1.agregarItem(herramienta1);
        orden1.agregarItem(herramienta2);

        OrdenDeVenta ordenOrdenadaPrecio = new OrdenDeVenta(horaFechaDeEmision,1);
        ordenOrdenadaPrecio.agregarItem(herramienta2);
        ordenOrdenadaPrecio.agregarItem(material1);
        ordenOrdenadaPrecio.agregarItem(herramienta1);

        Collections.sort(orden1.getItemsVendidos(), new ComparadorDeItemsPorPrecio());

        assertArrayEquals(ordenOrdenadaPrecio.getItemsVendidos().toArray(),
                 orden1.getItemsVendidos().toArray());
    }

    @Test
    public void calcularPrecioTotalDeOrden() throws ValorNegativoCeroException, ItemYaFigurandoEnOrdenException {
        BigDecimal precio = new BigDecimal(220.00);
        MaterialFabricado material1 = new MaterialFabricado("00", "Ladrillo", precio, 230,
                "Hecho con material de calidad");
        BigDecimal precio2 = new BigDecimal(2300.00);
        HerramientaElectrica herramienta1 = new HerramientaElectrica("122", "Taladro", precio2, 1, 500.25);
        orden1.agregarItem(material1);
        orden1.agregarItem(herramienta1);
        BigDecimal bD = new BigDecimal(63480); //2760 + 60720

        assertEquals(bD.setScale(2, RoundingMode.HALF_DOWN), orden1.precioTotal());
    }
}