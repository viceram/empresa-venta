package empresa.facturacion;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import org.junit.Before;
import org.junit.Test;

import empresa.excepcion.ItemYaFigurandoEnOrdenException;
import empresa.excepcion.OrdenExistenteException;
import empresa.excepcion.OrdenInexistenteExeption;
import empresa.excepcion.ValorNegativoCeroException;
import empresa.herramienta.HerramientaManual;

public class LibroDeFacturacionTest {

    LibroDeFacturacion libro;

    @Before
    public void before() {
        libro = new LibroDeFacturacion();
    }

    @Test
    public void agregarOrden() throws OrdenExistenteException {
        OrdenDeVenta orden = new OrdenDeVenta(LocalDateTime.now(), 1);
        libro.guardarUnaOrden(orden);
        assertEquals(1, libro.getCantidadDeOrdenesEmitidas());
    }

    @Test(expected = OrdenExistenteException.class)
    public void agregarOrdenExistente() throws OrdenExistenteException {
        OrdenDeVenta orden = new OrdenDeVenta(LocalDateTime.now(), 1);
        libro.guardarUnaOrden(orden);
        libro.guardarUnaOrden(orden);
    }

    @Test
    public void modificarOrden() throws OrdenExistenteException, OrdenInexistenteExeption,
            ItemYaFigurandoEnOrdenException, ValorNegativoCeroException {
        ItemParaVenta itemVendido = new HerramientaManual("14", "Taladro", new BigDecimal(2700.00), 1, "perforar");
        OrdenDeVenta orden = new OrdenDeVenta(LocalDateTime.of(2000, 11, 20, 15, 30), 1);
        orden.agregarItem(itemVendido);
        libro.guardarUnaOrden(orden);
        
        OrdenDeVenta ordenModificada = new OrdenDeVenta(LocalDateTime.now(), 1);
        ordenModificada.agregarItem(itemVendido);
        libro.modificarOrdenDeVenta(ordenModificada);
        assertEquals(ordenModificada, libro.getOrdenDeVenta(1));
    }

    @Test (expected = OrdenInexistenteExeption.class)
    public void modificarOrdenInexistente()
            throws OrdenExistenteException, OrdenInexistenteExeption{
        OrdenDeVenta orden = new OrdenDeVenta(LocalDateTime.of(2000, 11, 20, 15, 30), 1);
        libro.guardarUnaOrden(orden);
        OrdenDeVenta ordenModificada = new OrdenDeVenta(LocalDateTime.now(), 96);
        libro.modificarOrdenDeVenta(ordenModificada);
    }

    @Test
    public void eliminarOrden()
            throws OrdenExistenteException, OrdenInexistenteExeption{
        OrdenDeVenta orden = new OrdenDeVenta(LocalDateTime.of(2000, 11, 20, 15, 30), 1);
        libro.guardarUnaOrden(orden);
        libro.removerOrden(1);
        assertEquals(0, libro.getCantidadDeOrdenesEmitidas());
    }

    @Test (expected = OrdenInexistenteExeption.class)
    public void eliminarOrdenInexistente()
            throws OrdenExistenteException, OrdenInexistenteExeption{
        OrdenDeVenta orden = new OrdenDeVenta(LocalDateTime.of(2000, 11, 20, 15, 30), 1);
        libro.guardarUnaOrden(orden);
        libro.removerOrden(-1134);
    }
}
