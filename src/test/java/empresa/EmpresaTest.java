package empresa;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;
import empresa.material.*;
import empresa.excepcion.HerramientaYaRegistradaException;
import empresa.excepcion.ItemNoRegistradoException;
import empresa.excepcion.MaterialInexistenteException;
import empresa.excepcion.MaterialYaRegistradoException;
import empresa.excepcion.ValorNegativoCeroException;
import empresa.facturacion.ItemParaVenta;
import empresa.herramienta.*;

public class EmpresaTest {

    Empresa empresa;

    @Before
    public void before() {
        empresa = new Empresa("Prevedello", "3-4443-4");
    }

    @Test
    public void testAgregarMateriales() throws MaterialInexistenteException, MaterialYaRegistradoException,
            ValorNegativoCeroException {

        BigDecimal precio = new BigDecimal(400.00);
        Proveedor proveedor = new Proveedor("9-0002-1", "Corralon del constructor");
        MateriaPrima arena = new MateriaPrima("3233", "Arena", precio, 23, proveedor);

        empresa.agregarMaterial(arena);
        assertEquals(1, empresa.getListadoDeMateriales().size());
    }

    @Test
    public void testAgregarHerramientas() throws HerramientaYaRegistradaException, ValorNegativoCeroException {
        BigDecimal precio = new BigDecimal(400.00);
        HerramientaElectrica herramienta = new HerramientaElectrica("04", "Taladro", precio, 1, 320.00);

        empresa.agregarHerramientas(herramienta);
        assertEquals(1, empresa.getListadoDeHerramientas().size());
    }

    @Test
    public void testBuscarMaterial() throws MaterialInexistenteException, MaterialYaRegistradoException,
            ValorNegativoCeroException {

        BigDecimal precio = new BigDecimal(400.00);
        Proveedor proveedor = new Proveedor("9-0002-1", "Corralon del constructor");
        MateriaPrima arena = new MateriaPrima("03", "Arena", precio, 30, proveedor);
        empresa.agregarMaterial(arena);
        Material materialObtenido = empresa.getMaterial("M03");

        assertEquals(arena, materialObtenido);
    }

    @Test
    public void testModificarMaterial() throws MaterialInexistenteException, MaterialYaRegistradoException,
            ValorNegativoCeroException {

        BigDecimal precio = new BigDecimal(400.00);
        Proveedor proveedor = new Proveedor("9-0002-1", "Corralon del constructor");
        MateriaPrima arena = new MateriaPrima("03", "Arena", precio, 15, proveedor);
        empresa.agregarMaterial(arena);
        Proveedor proveedorModificado = new Proveedor("9-3302-1", "Corralon IECO");
        MateriaPrima materialModificado = new MateriaPrima("03", "Arena", precio, 15, proveedorModificado);
        empresa.modificarMaterial(materialModificado);

        Material materialObtenido = empresa.getMaterial("M03");

        assertEquals(materialModificado, materialObtenido);
    }

    @Test
    public void testEliminarMaterial()
            throws MaterialInexistenteException, MaterialYaRegistradoException, ValorNegativoCeroException {
        BigDecimal precio = new BigDecimal(400.00);
        Proveedor proveedor = new Proveedor("9-0002-1", "Corralon del constructor");
        MateriaPrima arena = new MateriaPrima("03", "Arena", precio, 33, proveedor);

        empresa.agregarMaterial(arena);
        empresa.eliminarMaterial("M03");
        assertEquals(0, empresa.getListadoDeMateriales().size());
    }

    @Test
    public void listaDeItemsDispVenta() throws MaterialYaRegistradoException, HerramientaYaRegistradaException,
            ValorNegativoCeroException {
        BigDecimal precio = new BigDecimal(400.00);
        Proveedor proveedor = new Proveedor("9-0002-1", "Corralon del constructor");
        MateriaPrima arena = new MateriaPrima("03", "Arena", precio, 33, proveedor);
        empresa.agregarMaterial(arena);

        BigDecimal precioH = new BigDecimal(400.00);
        HerramientaElectrica herramienta = new HerramientaElectrica("04", "Taladro", precioH, 1, 320.00);
        empresa.agregarHerramientas(herramienta);

        ArrayList<ItemParaVenta> iDispVta = new ArrayList<ItemParaVenta>();
        iDispVta.add(herramienta);
        assertArrayEquals(iDispVta.toArray(), empresa.getItemsDisponiblesVenta().toArray());
    }

    @Test
    public void obtenerItemDispVentaDeUnaListaDeEllos()
            throws MaterialYaRegistradoException, HerramientaYaRegistradaException, ItemNoRegistradoException,
            ValorNegativoCeroException {
        BigDecimal precio = new BigDecimal(400.00);
        Proveedor proveedor = new Proveedor("9-0002-1", "Corralon del constructor");
        MateriaPrima arena = new MateriaPrima("03", "Arena", precio, 33, proveedor);
        empresa.agregarMaterial(arena);

        BigDecimal precioH = new BigDecimal(400.00);
        HerramientaElectrica herramienta = new HerramientaElectrica("04", "Taladro", precioH, 
        1, 320.00);
        empresa.agregarHerramientas(herramienta);

        ItemParaVenta iV = Empresa.buscarUnItemParaVenta(empresa.getItemsDisponiblesVenta(), "H04");
        assertEquals(herramienta, iV);

    }

    @Test 
    public void listaDeItemsDisponibleParaVentaOrdenadaPorNombre() throws ValorNegativoCeroException,
            HerramientaYaRegistradaException {
        BigDecimal precioH = new BigDecimal(2000.00);
        HerramientaElectrica herramienta = new HerramientaElectrica("4", "Cortadora", precioH, 10, 320.00);
        empresa.agregarHerramientas(herramienta);

        HerramientaManual herramientaM = new HerramientaManual("1", "Martillo", new BigDecimal(500), 30, "Martillar");
        empresa.agregarHerramientas(herramientaM);

        HerramientaManual herramientaM2 = new HerramientaManual("2", "Alicates", new BigDecimal(450), 30, "Cortar/Sujetar");
        empresa.agregarHerramientas(herramientaM2);

        ArrayList<ItemParaVenta> itOrdNombre = new ArrayList<ItemParaVenta>();
        itOrdNombre.add(herramientaM2);
        itOrdNombre.add(herramienta);
        itOrdNombre.add(herramientaM);

        assertArrayEquals(itOrdNombre.toArray(), empresa.obtenerListaItemsParaVentaOrdenadaPorNombre().toArray());
    }

    @Test 
    public void listaDeItemsDisponibleParaVentaOrdenadaPorPrecio() throws ValorNegativoCeroException,
            HerramientaYaRegistradaException {
        HerramientaElectrica herramienta = new HerramientaElectrica("4", "Cortadora", new BigDecimal(2000.00), 10, 320.00);
        empresa.agregarHerramientas(herramienta);

        HerramientaManual herramientaM = new HerramientaManual("1", "Martillo", new BigDecimal(500), 30, "Martillar");
        empresa.agregarHerramientas(herramientaM);

        HerramientaManual herramientaM2 = new HerramientaManual("2", "Alicates", new BigDecimal(450), 30, "Cortar/Sujetar");
        empresa.agregarHerramientas(herramientaM2);

        ArrayList<ItemParaVenta> itOrdPrecio = new ArrayList<ItemParaVenta>();
        itOrdPrecio.add(herramientaM2);
        itOrdPrecio.add(herramientaM);
        itOrdPrecio.add(herramienta);

        assertArrayEquals(itOrdPrecio.toArray(), empresa.obtenerListaItemsParaVentaOrdenadaPorPrecio().toArray());
    }
}
