package herramienta;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;

import org.junit.Test;

import empresa.excepcion.ValorNegativoCeroException;
import empresa.herramienta.HerramientaManual;

public class HerramientaTest {
    @Test
    public void obtenerPrecioVta() throws ValorNegativoCeroException {
        HerramientaManual hM = new HerramientaManual("1", "Martillo", new BigDecimal(535), 7, "Martillar/clavar");
        BigDecimal bg = new BigDecimal(4494);
        assertEquals(bg.setScale(2), hM.getPrecio());
    }
}
