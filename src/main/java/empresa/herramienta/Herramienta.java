package empresa.herramienta;

import java.math.BigDecimal;

import empresa.Item;
import empresa.auxiliar.VerificadorDelValor;
import empresa.excepcion.ValorNegativoCeroException;
import empresa.facturacion.ItemParaVenta;
import java.math.RoundingMode;

public abstract class Herramienta extends Item implements ItemParaVenta, Comparable<Herramienta> {

    private BigDecimal precio;
    private Integer cantidad;

    public Herramienta(String codigo, String nombre, BigDecimal precio, Integer cantidad)
            throws ValorNegativoCeroException {
        super(codigo, nombre);
        VerificadorDelValor.rechazarPrecioNegativo(precio);
        this.precio = precio;
        VerificadorDelValor.rechazarPrecioNegativo(cantidad);
        this.cantidad = cantidad;
    }

    @Override
    public BigDecimal getPrecioBase() {
		return precio;
	}

	public void setPrecio(BigDecimal precio) {
		this.precio = precio;
    }
    
    @Override
    public String getCodigo(){
        return "H".concat(getCodigoGenericoDelItem());
    }

    public BigDecimal getPrecio(){
        BigDecimal ganancia = new BigDecimal(0.20);
        BigDecimal precioVenta = precio.add(precio.multiply(ganancia));
        BigDecimal precioVentaTotal = precioVenta.multiply(new BigDecimal(cantidad));
        return precioVentaTotal.setScale(2, RoundingMode.HALF_DOWN);
    }

    public Integer getCantidad(){
        return cantidad;
    }

    @Override
    public void setCantidad(Integer cantidad) throws ValorNegativoCeroException {
        VerificadorDelValor.rechazarPrecioNegativo(cantidad);
        this.cantidad = cantidad;
    }
    
} 
