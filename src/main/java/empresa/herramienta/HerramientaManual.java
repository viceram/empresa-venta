package empresa.herramienta;

import java.math.BigDecimal;

import empresa.excepcion.ValorNegativoCeroException;

public class HerramientaManual extends Herramienta {
    private String funcion;

    public HerramientaManual(String codigo, String nombre, BigDecimal precio, Integer cantidad, String funcion)
            throws ValorNegativoCeroException {
        super(codigo, nombre, precio, cantidad);
        this.funcion=funcion;
    }

    public void setFuncion(String funcion){
        this.funcion=funcion;
    }

    public String getFuncion(){
        return funcion;
    }

    @Override
    public int compareTo(Herramienta herramienta) {
        return this.getNombre().compareTo(herramienta.getNombre());
    }

    @Override
    public String toString(){
        return "Codigo: " + getCodigo() + "\nNombre: " + getNombre() + "\nPrecio base/compra: " + getPrecioBase()
           + "\n Cantidad: " + getCantidad() + "\nFuncion: " + getFuncion();
    }
}
