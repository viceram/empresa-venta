package empresa.herramienta;

import java.math.BigDecimal;

import empresa.excepcion.ValorNegativoCeroException;

public class HerramientaElectrica extends Herramienta {

    private Double consumoDeEnergia; // agregar atributo funcion

    public HerramientaElectrica(String codigo, String nombre, BigDecimal precio, Integer cantidad,
            Double consumoDeEnergia) throws ValorNegativoCeroException {
        super(codigo, nombre, precio, cantidad);
        this.consumoDeEnergia = consumoDeEnergia;
    }

    public Double getConsumoDeEnergia() {
        return consumoDeEnergia;
    }

    public void setConsumoDeEnergia(Double consumoDeEnergia) {
        this.consumoDeEnergia = consumoDeEnergia;
    }

    @Override
    public int compareTo(Herramienta herramienta) {
        return this.getNombre().compareTo(herramienta.getNombre());
    }

    @Override
    public String toString(){
        return "Codigo: " + getCodigo() + "\nNombre: " + getNombre() + "\nPrecio base/compra: " + getPrecioBase()
           + "\n Cantidad: " + getCantidad() + "\nConsumo energetico (Kwh): " + getConsumoDeEnergia();
    }
}
