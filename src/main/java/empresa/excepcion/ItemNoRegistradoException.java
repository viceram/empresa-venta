package empresa.excepcion;

public class ItemNoRegistradoException extends Exception{
    public ItemNoRegistradoException(String codigo){
        super("No se ha encontrado un item de codigo (" + codigo + ")");
    }
}
