package empresa.excepcion;

public class HerramientaYaRegistradaException extends Exception {
    public HerramientaYaRegistradaException(String codigo){
        super("Ya se ha registrado una herramienta de codigo (" + codigo + ") anteriormente");
    }
}
