package empresa.excepcion;

public class MaterialInexistenteException extends Exception {
    public MaterialInexistenteException(String codigo){
        super("No se ha encontrado un material con codigo (" + codigo + ")");
    }
}
