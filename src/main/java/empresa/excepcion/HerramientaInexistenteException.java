package empresa.excepcion;

public class HerramientaInexistenteException extends Exception {
    public HerramientaInexistenteException(String codigo){
        super("No se ha encontrado un material con codigo (" + codigo + ")");
    }
}
