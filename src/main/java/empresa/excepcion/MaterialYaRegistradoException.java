package empresa.excepcion;

public class MaterialYaRegistradoException extends Exception {
    public MaterialYaRegistradoException(String codigo){
        super("Ya se ha registrado un material de codigo (" + codigo + ") anteriormente");
    }
}
