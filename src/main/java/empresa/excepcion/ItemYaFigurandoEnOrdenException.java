package empresa.excepcion;

public class ItemYaFigurandoEnOrdenException extends Exception{
    public ItemYaFigurandoEnOrdenException(String codigo){
        super("En esta orden ya hay un Item con el codigo (" + codigo + ") considere modificarla");
    }
}
