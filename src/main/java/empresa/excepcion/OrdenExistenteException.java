package empresa.excepcion;

public class OrdenExistenteException extends Exception{
    public OrdenExistenteException(Integer numero){
        super("Ya se ha guardado una factura con el numeror(" +  numero + ") anteriormente." +
                "\n No se ha podido guardar la orden");
    }
}
