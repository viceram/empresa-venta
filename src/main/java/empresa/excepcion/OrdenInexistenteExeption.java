package empresa.excepcion;

public class OrdenInexistenteExeption extends Exception{
    public OrdenInexistenteExeption(Integer numero){
        super("No se ha encontrado una orden de numero(" + numero + ")");
    }
}
