package empresa.excepcion;

public class ValorNegativoCeroException extends Exception {
    public ValorNegativoCeroException(){
        super("No es valido que ingrese un valor negativo o 0");
    }
}
