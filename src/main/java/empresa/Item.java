package empresa;

import java.math.BigDecimal;

public abstract class Item {
    
    private String codigo;
    private String nombre;

    public Item(String codigo, String nombre){
        this.codigo = codigo;
        this.nombre = nombre;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCodigoGenericoDelItem(){
        return codigo;
    }
    
    public abstract String getCodigo();

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public abstract Integer getCantidad();
    public abstract BigDecimal getPrecioBase();
}
