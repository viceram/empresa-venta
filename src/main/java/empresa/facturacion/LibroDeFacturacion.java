package empresa.facturacion;

import java.util.ArrayList;

import empresa.excepcion.*;

public class LibroDeFacturacion {
    
    private ArrayList<OrdenDeVenta> ordenesEmitidas;

    public LibroDeFacturacion(){
        ordenesEmitidas = new ArrayList<OrdenDeVenta>();
    }

    public ArrayList<OrdenDeVenta> getOrdenesEmitidas(){
        return ordenesEmitidas;
    }

    public void guardarUnaOrden(OrdenDeVenta ordenFacturada) throws OrdenExistenteException {
        for(OrdenDeVenta var: ordenesEmitidas){
            if(var.numeroDeOrdenIdentico(ordenFacturada)){
                throw new OrdenExistenteException(ordenFacturada.getNumero());
            }
        }
        ordenesEmitidas.add(ordenFacturada);
    }

    public OrdenDeVenta getOrdenDeVenta(Integer numero) throws OrdenInexistenteExeption {
        OrdenDeVenta ordenEncontrada = null;
        for(OrdenDeVenta var: ordenesEmitidas){
            if(var.getNumero().equals(numero)){
                ordenEncontrada = var;
            }
        }
        if(ordenEncontrada == null){
            throw new OrdenInexistenteExeption(numero);
        }
        return ordenEncontrada;
    }

    public void modificarOrdenDeVenta(OrdenDeVenta ordenModificada) throws OrdenInexistenteExeption,
                    OrdenExistenteException {
        OrdenDeVenta ordenEcontrada;
        ordenEcontrada = getOrdenDeVenta(ordenModificada.getNumero());
        this.ordenesEmitidas.remove(ordenEcontrada);
        this.guardarUnaOrden(ordenModificada);
    }

    public void removerOrden(Integer numero) throws OrdenInexistenteExeption {
        OrdenDeVenta ordenAEliminar;
        ordenAEliminar = getOrdenDeVenta(numero);
        ordenesEmitidas.remove(ordenAEliminar);
    }

    public int getCantidadDeOrdenesEmitidas(){
        return ordenesEmitidas.size();
    }
}
