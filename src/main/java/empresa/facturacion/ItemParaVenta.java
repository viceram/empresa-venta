package empresa.facturacion;

import java.math.BigDecimal;

import empresa.excepcion.ValorNegativoCeroException;

public interface ItemParaVenta { 
    public BigDecimal getPrecio();
    public String getNombre();
    public String getCodigo();
    public void setCantidad(Integer cantidad) throws ValorNegativoCeroException;
    public Integer getCantidad();
    public BigDecimal getPrecioBase();
}
