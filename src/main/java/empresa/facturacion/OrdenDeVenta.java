package empresa.facturacion;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import empresa.excepcion.*;
import java.math.RoundingMode;

public class OrdenDeVenta{

    private LocalDateTime horaFechaDeEmision;
    private Integer numero;
    private List<ItemParaVenta> itemsVendidos;


    public OrdenDeVenta(LocalDateTime horaFechaDeEmision, Integer numero){
        this.horaFechaDeEmision = horaFechaDeEmision;
        this.numero = numero;
        itemsVendidos = new ArrayList<ItemParaVenta>();
    }

    public void setHoraFecha(LocalDateTime horaFechaDeEmision){
        this.horaFechaDeEmision = horaFechaDeEmision;
    }

    public LocalDateTime getHoraFecha(){
        return horaFechaDeEmision;
    }

    public Integer getNumero(){
        return numero;
    }

    public List<ItemParaVenta> getItemsVendidos(){
        return itemsVendidos;
    }

    public ItemParaVenta getItem(String codigo) throws ItemNoRegistradoException{
        ItemParaVenta itemEncontrado = null;
        for(ItemParaVenta var: itemsVendidos){
            if(var.getCodigo().equals(codigo)){
                itemEncontrado=var;
            }
        }
        if(itemEncontrado == null){
            throw new ItemNoRegistradoException(codigo);
        }
        return itemEncontrado;
    }

    public void modificarItemVendido(ItemParaVenta itemModificado) throws ItemYaFigurandoEnOrdenException,
                        ItemNoRegistradoException{
        ItemParaVenta itemEncontrado = getItem(itemModificado.getCodigo());
        itemsVendidos.remove(itemEncontrado);
        this.agregarItem(itemModificado);
    }

    public void removerItem(String codigo) throws ItemNoRegistradoException {
        ItemParaVenta itemAEliminar = null;
        itemAEliminar = getItem(codigo);
        itemsVendidos.remove(itemAEliminar);
    }

    public void agregarItem(ItemParaVenta itemVendido)throws ItemYaFigurandoEnOrdenException{
        for(ItemParaVenta var: itemsVendidos){
            if(var.getCodigo().equals(itemVendido.getCodigo())){
                throw new ItemYaFigurandoEnOrdenException(itemVendido.getCodigo());
            }
        }
        this.itemsVendidos.add(itemVendido);
    }

    public BigDecimal precioTotal(){
        BigDecimal precioTotal = new BigDecimal(0.00);
        for(ItemParaVenta var: itemsVendidos){
            precioTotal = precioTotal.add(var.getPrecio());
        }
        return precioTotal.setScale(2, RoundingMode.HALF_DOWN);
    }

    public boolean numeroDeOrdenIdentico(OrdenDeVenta orden){
        return this.numero.equals(orden.getNumero());
    }

    public String imprimirOrden(){
        String listadoItems = "";
        for (ItemParaVenta var : itemsVendidos) {
            listadoItems = listadoItems + var.getCodigo() + "       " + var.getNombre() + "            " 
              + var.getPrecio() + "\n";
        }
        listadoItems = listadoItems + "                                 " + precioTotal();
        return "Codigo   -    Nombre         -   Precio \n" + listadoItems;

    }
}