package empresa.auxiliar;

import java.util.Comparator;

import empresa.herramienta.Herramienta;

public class ComparadorDeHerramientasPorPrecio implements Comparator<Herramienta> {
    
    @Override
    public int compare(Herramienta hta1, Herramienta hta2){
        return hta1.getPrecioBase().compareTo(hta2.getPrecioBase());
    }
}
