package empresa.auxiliar;

import java.util.Comparator;

import empresa.material.Material;

public class ComparadorDeMaterialesPorPrecio implements Comparator<Material>{
    
    @Override
    public int compare(Material mat1, Material mat2){
        return mat1.getPrecioBase().compareTo(mat2.getPrecioBase());
    }
}
