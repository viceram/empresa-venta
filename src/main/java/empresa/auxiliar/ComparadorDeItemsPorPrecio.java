package empresa.auxiliar;

import java.util.Comparator;

import empresa.facturacion.ItemParaVenta;

public class ComparadorDeItemsPorPrecio implements Comparator<ItemParaVenta>{
    
    @Override
    public int compare(ItemParaVenta item1, ItemParaVenta item2){
        return item1.getPrecioBase().compareTo(item2.getPrecioBase());
    }
}
