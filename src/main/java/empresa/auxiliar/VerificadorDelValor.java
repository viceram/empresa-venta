package empresa.auxiliar;

import java.math.BigDecimal;

import empresa.excepcion.ValorNegativoCeroException;

public class VerificadorDelValor {
    public static void rechazarPrecioNegativo(BigDecimal precioBase) throws ValorNegativoCeroException {
        if(precioBase.doubleValue() < 0 || precioBase.doubleValue() == 0 ){
            throw new ValorNegativoCeroException();
        }
    }
        
    public static void rechazarPrecioNegativo(Integer cantidad) throws ValorNegativoCeroException {
        if(cantidad < 0 || cantidad == 0 ){
            throw new ValorNegativoCeroException();
        }
    }
}
