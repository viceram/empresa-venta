package empresa;

import java.util.ArrayList;
import java.util.Collections;

import empresa.material.*;
import empresa.auxiliar.ComparadorDeHerramientasPorPrecio;
import empresa.auxiliar.ComparadorDeMaterialesPorPrecio;
import empresa.excepcion.*;
import empresa.facturacion.ItemParaVenta;
import empresa.facturacion.LibroDeFacturacion;
import empresa.herramienta.*;

public class Empresa {
    private String cuil;
    private String nombre;
    private ArrayList<Herramienta> listadoDeHerramientas;
    private ArrayList<Material> listadoDeMateriales;
    private LibroDeFacturacion libroDeOrdenes;

    public Empresa(String nombre, String cuil) {
        this.nombre=nombre;
        this.cuil=cuil;
        listadoDeHerramientas = new ArrayList<Herramienta>();
        listadoDeMateriales = new ArrayList<Material>();
        libroDeOrdenes = new LibroDeFacturacion();
    }

    public String getNombre() {
        return nombre;
    }

    public LibroDeFacturacion getLibroDeOrdenes(){
        return libroDeOrdenes;
    }

    public String getCuil() {
        return cuil;
    }

    public ArrayList<Material> getListadoDeMateriales() {
        return listadoDeMateriales;
    }

    public ArrayList<Herramienta> getListadoDeHerramientas() {
        return listadoDeHerramientas;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void agregarMaterial(Material material) throws MaterialYaRegistradoException {
        for(Material var: listadoDeMateriales){
            if(var.getCodigo().equals(material.getCodigo())){
                throw new MaterialYaRegistradoException(material.getCodigo());
            }
        }
        this.listadoDeMateriales.add(material);
    }

    public Material getMaterial(String codigo) throws MaterialInexistenteException {
        Material materialEncontrado = null;
        for(Material var: listadoDeMateriales){
            if(var.getCodigo().equals(codigo)){
                materialEncontrado = var;
            }
        }
        if(materialEncontrado == null){
            throw new MaterialInexistenteException(codigo);
        }
        return materialEncontrado;
    }

    public void modificarMaterial(Material materialModificado) throws MaterialInexistenteException,
            MaterialYaRegistradoException {
        Material materialEncontrado;
        materialEncontrado = getMaterial(materialModificado.getCodigo());
        listadoDeMateriales.remove(materialEncontrado);
        this.agregarMaterial(materialModificado);
    }

    public void eliminarMaterial(String codigo) throws MaterialInexistenteException {
        Material materialEncontrado;
        materialEncontrado = getMaterial(codigo);
        listadoDeMateriales.remove(materialEncontrado);
    }

    public Herramienta getHerramienta(String codigo) throws HerramientaInexistenteException {
        Herramienta herramientaEncontrada = null;
        for(Herramienta var: listadoDeHerramientas){
            if(var.getCodigo().equals(codigo)){
                herramientaEncontrada = var;
            }
        }

        if(herramientaEncontrada == null){
            throw new HerramientaInexistenteException(codigo);
        }
        return herramientaEncontrada;
    }

    public void modificarHerramienta(Herramienta herramientaModificada) throws HerramientaInexistenteException,
            HerramientaYaRegistradaException {
        Herramienta herramientaEncontrada;
        herramientaEncontrada = getHerramienta(herramientaModificada.getCodigo());
        listadoDeHerramientas.remove(herramientaEncontrada);
        this.agregarHerramientas(herramientaModificada);
    }

    public void eliminarHerramienta(String codigo) throws HerramientaInexistenteException {
        Herramienta herramientaEncontrada;
        herramientaEncontrada = getHerramienta(codigo);
        listadoDeHerramientas.remove(herramientaEncontrada);
    }

    public void agregarHerramientas(Herramienta herramienta) throws HerramientaYaRegistradaException {
        for(Herramienta var: listadoDeHerramientas){
            if(var.getCodigo().equals(herramienta.getCodigo())){
                throw new HerramientaYaRegistradaException(herramienta.getCodigo());
            }
        }
        this.listadoDeHerramientas.add(herramienta);
    }

    public void setCuil(String cuil){
        this.cuil = cuil;
    }
    
    public ArrayList<Item> getListaItemTotales(){
        ArrayList<Item> listaTotalDeItems = new ArrayList<Item>();
        listaTotalDeItems.addAll(getListadoDeMateriales());
        listaTotalDeItems.addAll(getListadoDeHerramientas());
        return listaTotalDeItems;
    }

    public ArrayList<ItemParaVenta> getItemsDisponiblesVenta(){
        ArrayList<ItemParaVenta> iDispVenta = new ArrayList<ItemParaVenta>();
        ArrayList<Item> items = getListaItemTotales();
        for(Item var: items){
            if(var.getClass().equals(HerramientaElectrica.class)){
                HerramientaElectrica her = (HerramientaElectrica) var;
                iDispVenta.add(her);
            }
            if(var.getClass().equals(HerramientaManual.class)){
                HerramientaManual her = (HerramientaManual) var;
                iDispVenta.add(her);
            }
            if(var.getClass().equals(MaterialFabricado.class)){
                MaterialFabricado mat = (MaterialFabricado) var;
                iDispVenta.add(mat);
            }
        }
        return iDispVenta;
    }

    public static ItemParaVenta buscarUnItemParaVenta(ArrayList<ItemParaVenta> iDispVta, String codigo)
            throws ItemNoRegistradoException {
        ItemParaVenta itemEncontrado = null;
        for(ItemParaVenta var: iDispVta){
            if(var.getCodigo().equals(codigo)){
                itemEncontrado = var;
            }
        }
        if(itemEncontrado == null){
            throw new ItemNoRegistradoException(codigo);
        }
        return itemEncontrado;
    }

    public void ordenarMaterialesPorNombres(){
        Collections.sort(listadoDeMateriales);
    }

    public void ordenarMaterialesPorPrecio(){
        Collections.sort(listadoDeMateriales, new ComparadorDeMaterialesPorPrecio());
    }

    public void ordenarHerramientasPorNombres(){
        Collections.sort(listadoDeHerramientas);
    }

    public void ordenarHerramientasPorPrecio(){
        Collections.sort(listadoDeHerramientas, new ComparadorDeHerramientasPorPrecio());
    }

    public ArrayList<ItemParaVenta> obtenerListaItemsParaVentaOrdenadaPorNombre(){
        ordenarMaterialesPorNombres();
        ordenarHerramientasPorNombres();
        ArrayList<ItemParaVenta> iDVta = getItemsDisponiblesVenta();
        return iDVta;
    }

    public ArrayList<ItemParaVenta> obtenerListaItemsParaVentaOrdenadaPorPrecio(){
        ordenarMaterialesPorPrecio();
        ordenarHerramientasPorPrecio();
        ArrayList<ItemParaVenta> iDVta = getItemsDisponiblesVenta();
        return iDVta;
    }

    public static Item getItem(ArrayList<Item> items, String codigo) throws ItemNoRegistradoException {
        Item itemEncontrado = null;
        for(Item it: items){
            if(it.getCodigo().equals(codigo)){
                itemEncontrado = it;
            }
        }
        if(itemEncontrado == null){
            throw new ItemNoRegistradoException(codigo);
        }
        return itemEncontrado;
    }
}
