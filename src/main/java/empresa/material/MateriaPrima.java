package empresa.material;

import java.math.BigDecimal;

import empresa.Proveedor;
import empresa.excepcion.ValorNegativoCeroException;

public class MateriaPrima extends Material {
    private Proveedor proveedor;

    public MateriaPrima(String codigo, String nombre, BigDecimal precio, Integer cantidadKg, Proveedor proveedor)
            throws ValorNegativoCeroException {
        super(codigo, nombre, precio, cantidadKg);
        this.proveedor = proveedor;
    }

    public Proveedor getProveedor(){
        return proveedor;
    }

    public void setProveedor(Proveedor proveedor) {
        this.proveedor = proveedor;
    }

    @Override
    public int compareTo(Material material) {
        return this.getNombre().compareTo(material.getNombre());
    }

    @Override
    public String toString(){
        return "Codigo: " + getCodigo() + "\nNombre: " + getNombre() + "\nPrecio base/compra: " + getPrecioBase()
           + "\n Cantidad: " + getCantidad() + "\n  Datos del Proveedor:" + "\nNombre: " + getProveedor().getNombreEmpresa()
           + "\nCUIT: " + getProveedor().getCuil();
    }
}
