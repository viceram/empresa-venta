package empresa.material;

import java.math.BigDecimal;

import empresa.Item;
import empresa.auxiliar.VerificadorDelValor;
import empresa.excepcion.ValorNegativoCeroException;

public abstract class Material extends Item implements Comparable<Material> {

    private BigDecimal precio;
    private Integer cantidad;

    public Material(String codigo, String nombre, BigDecimal precio, Integer cantidad)
            throws ValorNegativoCeroException {
        super(codigo, nombre);
        VerificadorDelValor.rechazarPrecioNegativo(precio);
        this.precio = precio;
        VerificadorDelValor.rechazarPrecioNegativo(cantidad);
        this.cantidad = cantidad;
    }

    public void setCodigo(BigDecimal precio){
        this.precio=precio;
    }

    public BigDecimal getPrecioBase(){
        return precio;
    }

    public Integer getCantidad(){
        return cantidad;
    }

    public String getCodigo(){
        return "M".concat(this.getCodigoGenericoDelItem());
    }
    
    public void setCantidad(Integer cantidad) throws ValorNegativoCeroException{
        VerificadorDelValor.rechazarPrecioNegativo(cantidad);
        this.cantidad = cantidad;
    }

    public abstract int compareTo(Material material);
}
