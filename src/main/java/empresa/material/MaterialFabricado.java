package empresa.material;

import java.math.BigDecimal;

import empresa.excepcion.ValorNegativoCeroException;
import empresa.facturacion.ItemParaVenta;
import java.math.MathContext;
import java.math.RoundingMode;

public class MaterialFabricado extends Material implements ItemParaVenta {

    private String descripcion;

    public MaterialFabricado(String codigo, String nombre, BigDecimal precio, Integer cantidad, String descripcion)
            throws ValorNegativoCeroException {
        super(codigo, nombre, precio, cantidad);
                this.descripcion = descripcion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    @Override
    public int compareTo(Material material) {
        return this.getNombre().compareTo(material.getNombre());
    }

    @Override
    public BigDecimal getPrecio(){
        BigDecimal ganancia = new BigDecimal(0.20);
        BigDecimal precioVenta = getPrecioBase().add(getPrecioBase().multiply(ganancia));
        BigDecimal precioVentaTotal = precioVenta.multiply(new BigDecimal(getCantidad()));
        return precioVentaTotal.setScale(2, RoundingMode.HALF_DOWN);
    }

    @Override
    public String toString(){
        return "Codigo: " + getCodigo() + "\nNombre: " + getNombre() + "\nPrecio base/compra: " + getPrecioBase()
           + "\n Cantidad: " + getCantidad() + "\nDescripcion" + getDescripcion();
    }
}
