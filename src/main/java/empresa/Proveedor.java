package empresa;

public class Proveedor {
    private String cuil;
    private String nombreEmpresa;

    public Proveedor(String cuil, String nombreEmpresa){
        this.cuil = cuil;
        this.nombreEmpresa = nombreEmpresa;
    }

    public String getCuil() {
        return cuil;
    }

    public String getNombreEmpresa() {
        return nombreEmpresa;
    }

    public void setNombreEmpresa(String nombreEmpresa) {
        this.nombreEmpresa = nombreEmpresa;
    }

    public void setCuil(String cuil) {
        this.cuil = cuil;
    }

    
}
